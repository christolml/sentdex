package main

/*
estructuras, receptores de valores en metodos
*/

import (
	"fmt"
)

const usixteenbitmax float64 = 65535
const kmh_multiple float64 = 1.60934

type car struct {
	gas_pedal      uint16 //min 0 mas 65535
	breake_pedal   uint16
	steering_wheel int16 //-32k  + 32k
	top_speed_kmh  float64
}

func (c car) kmh() float64 {
	return float64(c.gas_pedal) * (c.top_speed_kmh / usixteenbitmax)
}

func (c car) mph() float64 {
	return float64(c.gas_pedal) * (c.top_speed_kmh / usixteenbitmax / kmh_multiple)
}

func main() {
	a_car := car{
		gas_pedal:      22431,
		breake_pedal:   0,
		steering_wheel: 12543,
		top_speed_kmh:  225.0,
	}

	fmt.Println(a_car.top_speed_kmh)
	fmt.Println(a_car.kmh())
	fmt.Println(a_car.mph())
}
