package main

import (
	"fmt"
	"net/http"
)

//nuestro index lo que nos muestra
func index_handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Orale, Go es la onda!!! :0")
}

// lo que nos muestra en la pagina de about
func about_handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Diseño de la página about por Christopher lml")
}

func main() {
	//este nos manda a nuestro index del servidor
	http.HandleFunc("/", index_handler)
	// este nos manda como a otra pagina de nuestro servidor
	http.HandleFunc("/about", about_handler)
	// este crea nuestro server en localhost en el puerto 8000
	http.ListenAndServe(":8000", nil)

}
